package com.sist.dao;

import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import java.util.*;
public interface EmpMapper {
	// <resultMap><result property="" column="">
   @Results({
	   @Result(property="empno",column="empno"),
	   @Result(property="ename",column="ename"),
	   @Result(property="job",column="job"),
	   @Result(property="hiredate",column="hiredate"),
	   @Result(property="sal",column="sal"),
	   @Result(property="dvo.dname",column="dname"),
	   @Result(property="dvo.loc",column="loc")
   })
   @Select("SELECT empno,ename,job,hiredate,sal,dname,loc "
		  +"FROM emp,dept "
		  +"WHERE emp.deptno=dept.deptno")
   public List<EmpVO> empListData();
   
   @Results({
	   @Result(property="empno",column="empno"),
	   @Result(property="ename",column="ename"),
	   @Result(property="job",column="job"),
	   @Result(property="hiredate",column="hiredate"),
	   @Result(property="sal",column="sal"),
	   @Result(property="dvo.dname",column="dname"),
	   @Result(property="dvo.loc",column="loc")
   })
   @Select("SELECT e.* "
		  +"FROM (SELECT empno,ename,job,hiredate,sal,dname,loc,RANK() OVER(ORDER BY sal DESC) "
		  +"as rank FROM emp,dept "
		  +"WHERE emp.deptno=dept.deptno) e "
		  +"WHERE empno=#{empno}")
   public EmpVO empDetailData(int empno);
   
   @Select("SELECT empno,ename,job,sal FROM emp")
   public List<EmpVO> empAllData();
}








