package com.sist.r;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
/*
 *   <span  class="wrap_link" ><!-- 링크 하나일 때 link_type2 추가-->

										<a href="/moviedb/main?movieId=123374" class="link_desc #info">
										
		File file=new File("c:\\aaa\\a.json")
		
		FileWriter fw=new FileWriter(file);
		fw.write(arr.toJSONString());
		fw.close();
 */
public class MovieManager {
   public static void main(String[] args) {
	    MovieManager m=new MovieManager();
	    m.movieAllData();
   }
   public void movieAllData()
   {
	   MovieDAO dao = new MovieDAO();
	   try
	   {	
		   for(int i = 1;i<=7;i++) {
		   
		     int k=1;
            
		     Document doc=Jsoup.connect("https://movie.daum.net/premovie/scheduled?opt=reserve&page="+i).get();
		     Elements link=doc.select("span.wrap_link a.link_desc[href*=/moviedb/]");
		     
		     
		     for(int j=0;j<link.size();j++)
		     {
		    	try
		    	{
		    	
		    	 String dLink="https://movie.daum.net"+link.get(j).attr("href");
		    	 Document doc2=Jsoup.connect(dLink).get();
		    	 System.out.println(dLink);
		    	
		    	 Element title=doc2.selectFirst("div.mobile_subject strong.tit_movie");
		    	 
		    	 Element poster=doc2.selectFirst("span.thumb_summary img.img_summary");
		    	 Element regdate=doc2.select("dl.list_movie dd.txt_main").get(1);
		    	 Element grade=doc2.select("dl.list_movie dd").get(3);
		    	 Element genre=doc2.select("dl.list_movie dd.txt_main").get(0);
		    	 Element score=doc2.selectFirst("div.mobile_subject em.emph_grade");
		    	 Element actor=doc2.select("dd.type_ellipsis a").get(1);
		    	 Element director=doc2.select("dd.type_ellipsis a").get(0);
		    	 Element story=doc2.selectFirst("div.desc_movie");
		    	 
		    	 System.out.println(title.text());
		    	 System.out.println(poster.attr("src"));
		    	 System.out.println(regdate.text());
		    	 System.out.println(genre.text());
		    	 System.out.println(grade.text());
		    	 System.out.println(score.text());
		    	 System.out.println(actor.text());
		    	 System.out.println(director.text());
		    	 System.out.println(story.text());
		    	 System.out.println("================================================");
		    	 MovieVO vo=new MovieVO();
		    	 
		    	 vo.setMno(k);
		    	 vo.setTitle(title.text());
		    	 vo.setPoster(poster.attr("src"));
		    	 vo.setRegdate(regdate.text());
		    	 vo.setGenre(genre.text());
		    	 vo.setGrade(grade.text());
		    	 vo.setScore(score.text());
		    	 vo.setActor(actor.text());
		    	 vo.setDirector(director.text());
		    	 vo.setStory(story.text());
		    	 vo.setKey(youtubeGetData(title.text().substring(0,title.text().indexOf("("))));
		    	 dao.movieInsert(vo);
		    	 
		    	 k++;
		    	}catch(Exception ex){ex.printStackTrace();}
		     }
            
		   }
	   }catch(Exception ex)
	   {
		   ex.printStackTrace();
	   }
   }
   
   public String youtubeGetData(String title)
   {
	   String key="";
	   try
	   {
		   Document doc=Jsoup.connect("https://www.youtube.com/results?search_query="+title).get();
		   Pattern p=Pattern.compile("/watch\\?v=[^가-힣]+");// 특수문자 , 영문 , 숫자
		   // + 한글자 이상 
		   Matcher m=p.matcher(doc.toString()); // 빅데이터의 기본 (패턴)
		   /*
		    *    211.238.142.181  
		    *   (([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3})\\.([0-9]{1,3}))
		    *     group(1)       group(2)
		    */
		   while(m.find())
		   {
			   // /watch?v=PhZUQj4pbl0","webPageType":"WEB_PAGE_TYPE_WATCH"
			   String s=m.group();
			   s=s.substring(s.indexOf("=")+1,s.indexOf("\""));
			   key=s;
			   break;
		   }
		   /*
		    *     맛있는 , 맛있고 .....
		    *     맛있+
		    *     
		    *     짜다 짜고 ... 짜+
		    *     
		    *     ?
		    *     ^    ^[^]  []{1,3}
		    *     +
		    *     *
		    *     
		    *     [0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}
		    *     211.238.142.181
		    *     
		    */
	   }catch(Exception ex){}
	   return key;
   }

}
