package com.sist.r;
import java.net.InetSocketAddress;

import com.mongodb.*;
public class MovieDAO {
   private MongoClient mc;
   private DB db;
   private DBCollection dbc;
   public MovieDAO()
   {
	   try
	   {
		   mc=new MongoClient(new ServerAddress(new InetSocketAddress("127.0.0.1",27017)));
		   db=mc.getDB("mydb");
		   dbc=db.getCollection("movie2");
	   }catch(Exception ex){ex.printStackTrace();}
   }
   public void movieInsert(MovieVO vo)
   {
	   try 
	   {
		   BasicDBObject obj=new BasicDBObject();
		   //obj.put("cateno", vo.getCateno());
		   obj.put("mno", vo.getMno());
		   obj.put("title", vo.getTitle());
		   obj.put("poster", vo.getPoster());
		   obj.put("actor", vo.getActor());
		   obj.put("director", vo.getDirector());
		   obj.put("genre", vo.getGenre());
		   obj.put("grade", vo.getGrade());
		   obj.put("regdate", vo.getRegdate());
		   obj.put("score", vo.getScore());
		   obj.put("story", vo.getStory());
		   obj.put("key", vo.getKey());
		   dbc.insert(obj);
	   }catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
	   }
   }
}
