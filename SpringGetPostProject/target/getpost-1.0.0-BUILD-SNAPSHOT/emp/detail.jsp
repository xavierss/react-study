<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<style type="text/css">
.row{
   margin: 0px auto;
   width:400px;
}
</style>
</head>
<body>
   <div class="container">
     <h3 class="text-center">${vo.ename }님의 상세정보</h3>
     <div class="row">
       <table class="table table-hover">
         <tr>
           <th class="success text-right">사번</th>
           <td class="text-left">${vo.empno }</td>
         </tr>
         <tr>
           <th class="success text-right">이름</th>
           <td class="text-left">${vo.ename }</td>
         </tr>
         <tr>
           <th class="success text-right">직위</th>
           <td class="text-left">${vo.job }</td>
         </tr>
         <tr>
           <th class="success text-right">입사일</th>
           <td class="text-left"><fmt:formatDate value="${vo.hiredate }" pattern="yyyy-MM-dd"/></td>
         </tr>
         <tr>
           <th class="success text-right">부서명</th>
           <td class="text-left">${vo.dvo.dname }</td>
         </tr>
         <tr>
           <th class="success text-right">근무지</th>
           <td class="text-left">${vo.dvo.loc }</td>
         </tr>
         <tr>
           <th class="success text-right">급여</th>
           <td class="text-left">${vo.sal }</td>
         </tr>
         <tr>
           <th class="success text-right">급여순위</th>
           <td class="text-left">${vo.rank }</td>
         </tr>
       </table>
     </div>
   </div>
</body>
</html>






