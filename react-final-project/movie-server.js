const Client = require('mongodb').MongoClient;

const express = require('express');
const app = express();
const port = 3355;

app.listen(port, () => {
    console.log('Server Start!!', 'http://localhost:3355');
});

app.all('/*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

// /movie_release?page=1
app.get('/movie_release', (req, res) => {
    const page = req.query.page ? req.query.page : 1;
    const rowSize = 12;
    const skip = page * rowSize - rowSize;
    const url = 'mongodb://211.238.142.181:27017';

    Client.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection('movie').find({}).skip(skip).limit(rowSize).toArray((err, docs) => {
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs);
            conn.close();
        });
    });
});

app.get('/movie_schedule', (req, res) => {
    const page = req.query.page ? req.query.page : 1;
    const rowSize = 12;
    const skip = page * rowSize - rowSize;
    const url = 'mongodb://211.238.142.181:27017';

    Client.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection('movie2').find({}).skip(skip).limit(rowSize).toArray((err, docs) => {
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs);
            conn.close();
        });
    });
});

app.get('/movie_detail', (req, res) => {
    const mno = req.query.mno;
    const flag = req.query.flag;
    const url = 'mongodb://211.238.142.181:27017';
    let collection = '';

    if (flag === "release") {
        collection = 'movie';
    } else if (flag === "schedule") {
        collection = 'movie2';
    }

    Client.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection(collection).find({mno: Number(mno)}).toArray((err, docs) => {
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs[0]);
            conn.close();
        });
    });
});

// 영화 찾기
app.get('/movie_find', (req, res) => {
    const url = 'mongodb://211.238.142.181:27017';

    Client.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection('movie').find({}).toArray((err, docs) => {
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs);
            conn.close();
        });
    });
});

app.get('/movie_boxOffice', (req, res) => {
    const no = req.query.no;
    const url = 'mongodb://211.238.142.181:27017';

    let dname = '';
    if (no === '1') {
        dname = 'movie_w';
    } else if (no === '2') {
        dname = 'movie_m';
    } else if (no === '3') {
        dname = 'movie_y';
    }

    Client.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection(dname).find({}).toArray((err, docs) => {
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs);
            conn.close();
        });
    });
});

app.get('/movie_news', (req, res) => {
    const url = 'mongodb://211.238.142.181:27017';

    Client.connect(url, {useNewUrlParser: true, useUnifiedTopology: true}, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection('news').find({}).toArray((err, docs) => {
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs);
            conn.close();
        });
    });
});

