import React, {Component} from 'react';
import Axios from 'axios';

class MovieSearch extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ss: "",
            movie_date: [],
            serverUrl: 'http://localhost:3355'
        };

        this.handleUserInput = this.handleUserInput.bind(this);
    }

    handleUserInput(ss) {
        this.setState({ss: ss});
        console.log("ss=" + ss);
    }

    componentWillMount() {
        Axios.get(this.state.serverUrl + '/movie_find',
            {
                params: {
                    ss: this.state.ss
                }
            }
        ).then((response) => {
            this.setState({movie_data: response.data});
        });
    }

    render() {
        return (
            <div className="container-fluid text-center bg-grey">
                <h2>영화 찾기</h2><br/>
                <div className="row text-center">
                    <SearchBar ss={this.state.ss} onUserInput={this.handleUserInput}/>
                    <MovieTable movie_data={this.state.movie_data} ss={this.state.ss}/>
                </div>
                <br/>
            </div>
        );
    }
}

class MovieTable extends Component {
    render() {
        let rows = [];
        this.props.movie_data && this.props.movie_data.forEach((m, index) => {
            if (m.title.indexOf(this.props.ss) === -1) {
            } else {
                rows.push(<MovieRow movie_data={m} key={index}/>);
            }
        });

        return (
            <table className={"table"}>
                <thead>
                <tr>
                    <th></th>
                    <th>영화명</th>
                    <th>감독</th>
                    <th>출연</th>
                    <th>장르</th>
                </tr>
                </thead>
                <tbody>
                {rows}
                </tbody>
            </table>
        );
    }
}

class MovieRow extends Component {
    render() {
        return (
            <tr>
                <td><img src={this.props.movie_data.poster} alt={this.props.movie_data.title} width={"35px"} height={"35px"}/></td>
                <td>{this.props.movie_data.title}</td>
                <td>{this.props.movie_data.director}</td>
                <td>{this.props.movie_data.actor}</td>
                <td>{this.props.movie_data.genre}</td>
            </tr>
        );
    }
}

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.handlerChange = this.handlerChange.bind(this);
    }

    handlerChange(e) {
        this.props.onUserInput(e.target.value);
    }

    render() {
        return (
            <form>
                <input type={"text"} className={"input-sm"} placeholder={"Search"} value={this.props.ss}
                       onChange={this.handlerChange} size={"30"}/>
            </form>
        );
    }
}

export default MovieSearch;