import React, {Component} from 'react';

class Chat extends Component {
    render() {
        return (
            <div id={"chat_container"}>
                <div id="chat" className='active'>
                    <header><h1>Chat</h1></header>
                    <section className="content">
                        <div className="message_content">
                            <div className={"message_right"}>
                                <div className={"message-text"}>Hello</div>
                            </div>
                        </div>
                    </section>
                    <form action="">
                        <input id="input_chat" type="text"/>
                    </form>
                </div>
            </div>
        );
    }
}

export default Chat;