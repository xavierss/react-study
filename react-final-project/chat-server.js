const express = require('express');
const app = express();
const port = 3001;

const server = require('http').createServer(app);
const socketio = require('socket.io');
const io = socketio.listen(server);

server.listen(port, () => {
    console.log('Server Start!!', 'http://localhost:3001');
});

app.use('/public', express.static('./public'));

app.get('/', (req, res) => {
    res.redirect(302, './public');
});

io.on('connection', (socket) => {
    socket.on('chat-msg', (msg) => {
        console.log('message', msg);
        io.emit('chat-msg', msg);
    })
});