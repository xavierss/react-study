import React, {Component} from "react";
import Axios from 'axios';

/**
 React, Vue = > 상태관리
 ==데이터 관리==
 => props : 속성값 <MovieList list="">
 => state : 변경이 가능, 서버에서 들어오는 데이터 처리
 => refs : input, select...
 */

class MovieList extends Component {
    // 1.변수 => NodeJS에서 넘겨준 데이터를 저장
    constructor(props) {
        super(props);
        // 변수선언
        /*this.state = {
            movie: [],
            page: 1,
            data: '',
            detail: {}
        }*/
        this.state = {
            movie: [],
            url: 'http://localhost:3355',
            page: 1,
        };
        // 이벤트 등록
        this.prevHandler = this.prevHandler.bind(this);
        this.nextHandler = this.nextHandler.bind(this); // 권장사항
        /**
         * constructor() => componentWillMount() => render() => componentDidMount() => setState() => render()
         * */

    }

    prevHandler() {
        this.setState({page: this.state.page > 1 ? this.state.page - 1 : this.state.page});
        this.getData();
    }

    nextHandler() {
        this.setState({page: this.state.page < 6 ? this.state.page + 1 : this.state.page});
        this.getData();
    }

    getData() {
        Axios.get(this.state.url + '/movie', {params: {page: this.state.page}})
            .then((response) => {
                this.setState({movie: response.data});
            });
    }

    componentWillMount() {
        // 데이터 읽기 => state
        this.getData();
    }

    render() {
        // state 저장된 데이터 출력
        const html = this.state.movie.map((m) => {
            return (<div className="col-md-3" key={m.mno}>
                <div className="thumbnail">
                    <img src={m.poster} alt="Lights" style={{"width": "100%"}}/>
                    <div className="caption">
                        <p>{m.title}</p>
                    </div>
                </div>
            </div>)
        });

        return (
            <div className={"row"}>
                {html}
                <div className={"text-center col"}>
                    <input type={"button"} value={"이전"} className={"btn btn-lg btn-danger"} onClick={this.prevHandler}/>
                    <input type={"button"} value={"다음"} className={"btn btn-lg btn-primary"}
                           onClick={this.nextHandler}/>
                </div>
            </div>
        )
    }

    componentDidMount() {
        // Jquery, AngularJS, VueJS 연동
    }
}

export default MovieList;