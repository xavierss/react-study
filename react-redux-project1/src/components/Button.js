import React, {Component} from 'react';

class Button extends Component {
    constructor(props) {
        super(props);
        this.onIncrement = this.onIncrement.bind(this);
        this.onDecrement = this.onDecrement.bind(this);
    }

    onIncrement() {
        this.props.onIncrement();
    }

    onDecrement() {
        this.props.onDecrement();
    }

    render() {
        return (
            <div className={"row text-center"}>
                <button className={"btn btn-sm btn-primary"} onClick={this.onIncrement}>+</button>
                <button className={"btn btn-sm btn-success"} onClick={this.onDecrement}>-</button>
            </div>
        );
    }
}

export default Button;