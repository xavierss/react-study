import React, {Component} from 'react'
import $ from 'jquery'

class App3 extends Component {
    constructor(props) {
        super(props);
        console.log("constructor Call...");
    }

    componentWillMount() {
        console.log("componentWillMount() Call...");
    }

    render() {
        console.log("render() Call...");
        const html = this.props.music.map((m) => (
            <tr>
                <td>{m.rank}</td>
                <td><img src={m.poster} width={"30"} height={"30"}/></td>
                <td>{m.title}</td>
                <td>{m.singer}</td>
            </tr>
        ));

        return (
            <div>
                {/*이름 : {this.props.name} <br />
                나이 : {this.props.age} <br />
                주소 : {this.props.addr}*/}

                <table className={"table"}>
                    <tbody>
                    <tr>
                        <td>
                            <input type={"text"} id={"keyWord"} size={"20"} className={"input-sm"}/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table className={"table table-hover"} id={"user-table"}>
                    <thead>
                    <tr className={"table-danger"}>
                        <th>순위</th>
                        <th></th>
                        <th>노래명</th>
                        <th>가수명</th>
                    </tr>
                    </thead>
                    <tbody>
                    {html}
                    </tbody>
                </table>
            </div>
        )
    }

    componentDidMount() {
        console.log("componentDidMount() Call...");

        $('#keyWord').keyup(function () {
            let k = $(this).val().toUpperCase();

            let $userTableTbodyTr = $('#user-table > tbody > tr');
            $userTableTbodyTr.hide();
            // let $temp = $userTableTbodyTr.find('> td:nth-child(4n+3):contains("' + k + '")');
            let $temp = $userTableTbodyTr.find('> td:nth-child(4n+3)');
            $temp = $temp.filter((index,td) => {
                return td.innerHTML.toUpperCase().includes(k);
            })

            $temp.parent().show();
        })
    }
}

export default App3;