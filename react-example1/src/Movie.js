import React, {Component} from 'react'
import axios from 'axios'

class Movie extends Component {
    constructor(props) {
        super(props);
        // 변경된 데이터 받기 => state, props, refs(참조데이터 가져옴)
        this.state = {
            movie: []
        }
    }

    componentWillMount() {
        // 데이터를 읽어서 state에 저장
        axios.get('http://localhost:3000/json/rank.json').then((result) => {
            console.log(result);
            this.setState({movie: result.data.datas});
        })
    }

    render() {
        // 저장된 데이터를 화면에 출력
        const html = this.state.movie.map((m) => (
            <tr key={'movie-' + m.rank}>
                <td>{m.rank}</td>
                <td>{m.title}</td>
            </tr>
        ));
        return (
            <table className={"table table-bordered"}>
                <thead>
                <tr className={"success"}>
                    <th>순위</th>
                    <th>영화명</th>
                </tr>
                </thead>
                <tbody>
                {html}
                </tbody>
            </table>
        )
    }
}

export default Movie;