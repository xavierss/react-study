import React, {Component} from 'react';
import {decrement, increment} from "../actions";

class Button extends Component {
    constructor(props) {
        super(props);
        this.onIncrement = this.onIncrement.bind(this);
        this.onDecrement = this.onDecrement.bind(this);
    }

    onIncrement() {
        this.props.store.dispatch(increment());
    }

    onDecrement() {
        this.props.store.dispatch(decrement());
    }

    render() {
        return (
            <div>
                <button onClick={this.onIncrement}>+</button>
                <button onClick={this.onDecrement}>-</button>
            </div>
        );
    }
}

export default Button;