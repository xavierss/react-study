import React, {Component} from 'react';
import './App.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Header from "./Header";
import Footer from "./Footer";
import Home from "./components/movie/Home";
import Released from "./components/movie/Released";
import Scheduled from "./components/movie/Scheduled";
import BoxOffice from "./components/movie/BoxOffice";
import News from "./components/movie/News";

// Main Page
/**
 *  @pathvariable
 *  a.do?no=1&id=admin
 *  a.do/1/admin = a.do/:no/:id
 */

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Header/>
                    <Switch>
                        <Route exact path={"/"} component={Home}/>
                        <Route path={"/released"} component={Released}/>
                        <Route path={"/scheduled"} component={Scheduled}/>
                        <Route path={"/boxOffice"} component={BoxOffice}/>
                        <Route path={"/news"} component={News}/>
                    </Switch>
                    <Footer/>
                </div>
            </Router>
        );
    }
}

export default App;
