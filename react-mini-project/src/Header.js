import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                        </button>
                        <a className="navbar-brand" href="#">React Movie</a>
                    </div>
                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="nav navbar-nav navbar-right">
                            <li><NavLink to={"/"}>HOME</NavLink></li>
                            <li><NavLink to={"/released"}>현재상영영화</NavLink></li>
                            <li><NavLink to={"/scheduled"}>개봉예정영화</NavLink></li>
                            <li><NavLink to={"/boxOffice"}>박스오피스</NavLink></li>
                            <li><NavLink to={"/news"}>커뮤니티</NavLink></li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Header;