// 몽고DB Client
const Client = require('mongodb').MongoClient;
// 서버 설정
const express = require('express');
/**
 * ECMA 5.0
 * ECMA 6.0 =>  1) 변수 const,let
 *              2) 화살표함수 =>
 *              function,return 생략
 *
 *              spring5.0 람다식 강조
 *
 *              jsx = > javascript + XML
 */
const app = express();
const port = 3355;

// 서버 가동
app.listen(port, ()=> {
    console.log('Server Start!!', 'http://localhost:3355');
});

app.get('/', (req, res) => {

});

// /released?page=1
app.get('/released', (req, res) => {
    const page = req.query.page ? req.query.page : 1;
    const rowSize = 12;
    const skip = page * rowSize - rowSize;
    const url = 'mongodb://127.0.0.1:27017';

    Client.connect(url, (err, conn) => {
        const db = conn.db('mydb');
        // 연결
        // 몽고DB는 배열이 아님
        db.collection('movie').find({}).skip(skip).limit(rowSize).toArray((err, docs) => {
            console.log(docs);
            // 클라이언트 전송 => res (요청자의 ip) => Socket
            res.json(docs);
            conn.close();
        });
    });
});

app.get('/scheduled', (req, res) => {

});

app.get('/boxOffice', (req, res) => {

});

app.get('/news', (req, res) => {

});

/*
app.get('/',(req,res)=>{

});*/
