const express = require('express');
const app = express();
// port 번호 0 ~ 65535 => (0 ~ 1024), 7000,8080 알려진 포트들
const port = 3355;
const request = require('request');

/**
 * 서버 가동
 */
app.listen(port, () => {
    console.log('Server Start!!', 'http://localhost:3355');
});

/**
 * cross domain
 */
app.all('/*', function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    next();
});

app.get('/movie_info', (req, res) => {
    // 사용자 정보
    const no = req.query.no;
    let site = '';

    if (no === '1') {
        // 일일 박스 오피스
        site = 'searchMainDailyBoxOffice.do';
    } else if (no === '2') {
        // 실시간 예매율
        site = 'searchMainRealTicket.do';
    } else if (no === '3') {
        // 좌석점유율순위
        site = 'searchMainDailySeatTicket.do';
    } else if (no === '4') {
        // 온라인상영관 일일
        site = 'searchMainOnlineDailyBoxOffice.do';
    }
    let url = 'http://www.kobis.or.kr/kobis/business/main/' + site;

    request({url: url},(err,request,json)=> {
        // console.log(json);
        res.json(JSON.parse(json))
    });
});