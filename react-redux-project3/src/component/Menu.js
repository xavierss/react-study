import React, {Component} from 'react';
import {boxOffice, rMovie, sMovie} from "../actions";

class Menu extends Component {
    constructor(props) {
        super(props);
        this.onRmovie = this.onRmovie.bind(this);
        this.onBoxOffice = this.onBoxOffice.bind(this);
        this.onSmovie = this.onSmovie.bind(this);
    }

    onRmovie() {
        this.props.store.dispatch(rMovie());
    }

    onBoxOffice() {
        this.props.store.dispatch(boxOffice());
    }

    onSmovie() {
        this.props.store.dispatch(sMovie());
    }

    render() {
        return (
            <div>
                <button onClick={this.onRmovie} className={"btn btn-sm btn-primary"}>현재상영영화</button>
                <button onClick={this.onBoxOffice} className={"btn btn-sm btn-success"}>박스오피스</button>
                <button onClick={this.onSmovie} className={"btn btn-sm btn-danger"}>개봉예정영화</button>
            </div>
        );
    }
}

export default Menu;