import {R_MOVIE, BOX_OFFICE, S_MOVIE} from '../actions';
import {combineReducers} from 'redux';

/*
    store => action, state
 */
const movieInitialSate = {
    movie: [
        {
            "title": "조커 (2019)",
            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F0357a82b7226464b87072c0b8d2246b71567986846719"
        },
        {
            "title": "가장 보통의 연애 (2019)",
            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F08603eebe4e740e9a19384f0a0f5cfed1568165443925"
        },
        {
            "title": "제미니 맨 (2019)",
            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc81edb5c128f4975a41f19e0a8d0459e1567474338459"
        },
        {
            "title": "퍼펙트맨 (2019)",
            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fb894c77b8a82483e97bcd05fc2b2b93a1568784956298"
        },
        {
            "title": "소피와 드래곤: 마법책의 비밀 (2018)",
            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F363531a765b34d81b70a8c4356352cba1567673491779"
        },
        {
            "title": "장사리 : 잊혀진 영웅들 (2019)",
            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc6a267ee7c0b4fe4a783c189560cf2971566348594121"
        }
    ]
};

const movieReducer = (state = movieInitialSate, action) => {
        switch (action.type) {
            case R_MOVIE:
                return {
                    movie: [
                        {
                            "title": "조커 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F0357a82b7226464b87072c0b8d2246b71567986846719"
                        },
                        {
                            "title": "가장 보통의 연애 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F08603eebe4e740e9a19384f0a0f5cfed1568165443925"
                        },
                        {
                            "title": "제미니 맨 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc81edb5c128f4975a41f19e0a8d0459e1567474338459"
                        },
                        {
                            "title": "퍼펙트맨 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fb894c77b8a82483e97bcd05fc2b2b93a1568784956298"
                        },
                        {
                            "title": "소피와 드래곤: 마법책의 비밀 (2018)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F363531a765b34d81b70a8c4356352cba1567673491779"
                        },
                        {
                            "title": "장사리 : 잊혀진 영웅들 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc6a267ee7c0b4fe4a783c189560cf2971566348594121"
                        }
                    ]
                };
            case BOX_OFFICE:
                return {
                    movie: [
                        {
                            "title": "007 노 타임 투 다이 (2020)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F372367abb28d40eda280d00bee12d24d1570684696844"
                        },
                        {
                            "title": "10 미니츠 곤 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fcd2d4ccb636548eeaba9b392cfeba74b1566126023373"
                        },
                        {
                            "title": "제미니 맨 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc81edb5c128f4975a41f19e0a8d0459e1567474338459"
                        },
                        {
                            "title": "퍼펙트맨 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fb894c77b8a82483e97bcd05fc2b2b93a1568784956298"
                        },
                        {
                            "title": "소피와 드래곤: 마법책의 비밀 (2018)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F363531a765b34d81b70a8c4356352cba1567673491779"
                        },
                        {
                            "title": "장사리 : 잊혀진 영웅들 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc6a267ee7c0b4fe4a783c189560cf2971566348594121"
                        }
                    ]
                };
            case S_MOVIE:
                return {
                    movie: [
                        {
                            "title": "극한직업 (2018)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F4e00e81f2b6f4d2eb65b3387240cc3c01547608409838"
                        },
                        {
                            "title": "분노의 질주: 홉스&쇼 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F68697a4b31e7461b8ffe3211a9cd12b31564105313046"
                        },
                        {
                            "title": "제미니 맨 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc81edb5c128f4975a41f19e0a8d0459e1567474338459"
                        },
                        {
                            "title": "퍼펙트맨 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fb894c77b8a82483e97bcd05fc2b2b93a1568784956298"
                        },
                        {
                            "title": "소피와 드래곤: 마법책의 비밀 (2018)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2F363531a765b34d81b70a8c4356352cba1567673491779"
                        },
                        {
                            "title": "장사리 : 잊혀진 영웅들 (2019)",
                            "poster": "//img1.daumcdn.net/thumb/C155x225/?fname=http%3A%2F%2Ft1.daumcdn.net%2Fmovie%2Fc6a267ee7c0b4fe4a783c189560cf2971566348594121"
                        }
                    ]
                };
            default:
                return state;
        }
    }
;

const movieApp = combineReducers({
    movieReducer
});
export default movieApp;